from flask import Flask, render_template, request, redirect, url_for
from flask_pymongo import pymongo
from bson.objectid import ObjectId

app = Flask(__name__)

CONNECTION_STRING = "mongodb+srv://root:root@cluster0-bxi02.mongodb.net/test?retryWrites=true&w=majority"
client = pymongo.MongoClient(CONNECTION_STRING)
db = client.get_database('todo')
task_collection = pymongo.collection.Collection(db, 'tasks')

@app.route('/', methods=['GET', 'POST']) 
def index():
    msg = task_collection.find({})
    tasks = []
    for tache in msg:
        tasks.append({
            'msg': tache['msg'],
            'key': tache['_id']
        })
    return render_template('home.html', tasks= tasks)

@app.route('/add_task')
def add_task():
    return render_template('addTask.html')

@app.route('/add', methods =['POST']) 
def add():
    todo = request.form['todoitem']
    db.tasks.insert_one({"msg": todo})
    return redirect('/') 


@app.route('/edit_task/<key>')
def edit_task(key):
    the_task = db.tasks.find_one({"_id": ObjectId(key)})
    return render_template('editTask.html', task=the_task)

@app.route('/update_task', methods=['POST'])
def update_task():
    edit = request.form['edit']
    key = request.form['key']
    db.tasks.update({'_id': ObjectId(key)}, {"msg": edit})
    return redirect(url_for('index'))


@app.route('/delete_task/<key>')
def delete_task(key):
    db.tasks.remove({"_id": ObjectId(key)})
    return redirect(url_for('index'))